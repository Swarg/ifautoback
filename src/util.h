#ifndef UTIL_H_SENTRY
#define UTIL_H_SENTRY

/* #define DEBUG */

#define BUFFER_SIZE 4096

int file_exists(const char *filename);

int copy(const char *src, const char *dst);

long get_last_modifedtime(const char *path);

#endif
