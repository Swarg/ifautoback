#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <time.h>
#include <syslog.h>

#include "util.h"


/*
 ps -ax | grep ./ifautoback
 cat /var/run/ifautoback.pid
 kill -s SIGUSR1 <pid>
 kill -s SIGKILL <pid>    -- stop the programm
 */

time_t start_time;               /* keep uptime */
volatile sig_atomic_t sigusr1;   /* simple counter */

enum {
    IDLE_INTERVAL = 1 * 60,      /* 1 MIN sleep before check lm of config file */
    WAIT_BEFORE_BACKUP = 2 * 60, /* 2 MIN wait before autorestore config file */
    PID_BUFSIZE = 64
};

/* Observed config file */
const char *nw_config = "/etc/network/interfaces";
const char *cache_dir = "/var/cache/ifautoback";
const char *nw_backup = "/var/cache/ifautoback/interfaces.back";
const char *new_nw_config_back = "/var/cache/ifautoback/interfaces.new";

/* Commands to restrat Network: /etc/network/interfaces */
const char *nw_script = "/etc/init.d/networking";
const char *nw_stop = "/etc/init.d/networking stop";
const char *nw_start = "/etc/init.d/networking start";

/* variant 'B' if file /etc/init.d/networking not exists */
const char *nw_ifdown_all = "ifdown -a --exclude=lo";
const char *nw_ifup_all = "ifup -a --exclude=lo";
/* strerror(errno) */

void sigusr_handler(int s)
{
    signal(SIGUSR1, &sigusr_handler);
    if (s == SIGUSR1) {
        sigusr1++;
    }
}

/* to wakeup a sleeping process inside sleep-function */
void sigusr2_handler()
{
    signal(SIGUSR2, &sigusr_handler);
}

void update_pid_file()
{
    int fd;
    fd = open("/var/run/ifautoback.pid", O_CREAT|O_WRONLY|O_TRUNC, 0644);/*O_APPEND*/
    if (fd == -1) {
        syslog(LOG_ERR, "Cannot create pid-file in /var/run/");
    } else {
        char buf[PID_BUFSIZE];
        int len;
        len = sprintf(buf, "%d\n", getpid());
        write(fd, buf, len);
        close(fd);
    }
}

void daemonization()
{
    int pid;
    close(0); close(1); close(2);
    open("/dev/null", O_RDONLY);    /* stdin */
    open("/dev/null", O_WRONLY);    /* stdout */
    open("/dev/null", O_WRONLY);    /* stderr */
    chdir("/");
    pid = fork();   /* sure can create new session */
    if (pid > 0) {  /* change pid close parent */
        exit(0);
    }
    setsid();
    pid = fork();   /* not a leaver of session for keep rnd contr terminal*/
    if (pid > 0) {  /* change pid close parent */
        exit(0);
    }
}

int mk_backup()
{
    struct stat st = {0};

    if (stat(cache_dir, &st) == -1) {
        if (mkdir(cache_dir, S_IRWXU | S_IRWXG | S_IRWXO) == -1) {
            /* no permission to create dir */
            return -1;
        }
    }

    if (copy(nw_config, nw_backup)) {
        syslog(LOG_INFO, "%s backup saved: %s", nw_config, nw_backup);
        return 1;
    } else {
        syslog(LOG_ERR, "Cannot create backup: %s", nw_backup);
    }
    return 0;
}

/*
 * Check is network works correctly.
 * If not - restore backuped nw config and restart network
 * Now new network config will not be auto restored only if daemon process
 * receives a SIGUSR1
 */
int check_is_new_config_ok(int *last_n)
{
    /* TODO auto check is network works correctly */
    if (last_n && sigusr1 > *last_n) {
        *last_n = sigusr1;
        return 1;
    }
    return 0;
}

/*
stackoverflow.com/questions/52171796/how-to-restart-network-interface-in-debian-based-linux-without-being-restarting
*/
int restore_backup_and_restart_network()
{
    if (!file_exists(nw_backup)) {
        syslog(LOG_ERR, "Canceled. Backup not found! %s", nw_backup);
    }
    else {
        int ec1, ec2;
        const char *cmd_stop;
        const char *cmd_start;

        /* current not confirmed config - place to cache */
        copy(nw_config, new_nw_config_back);

        if (!copy(nw_backup, nw_config)) {
            return 0;
        }
        syslog(LOG_INFO, "Interface config Restored");

        /* Restart Network to last worked config */

        /* sudo /etc/init.d/networking restart */
        if (file_exists(nw_script)) {
            cmd_stop = nw_stop;
            cmd_start = nw_start;
        } else {
            cmd_stop = nw_ifdown_all;
            cmd_start = nw_ifup_all;
        }

        ec1 = system(cmd_stop);
        ec2 = system(cmd_start);

        if (ec1 != 0) {
            syslog(LOG_ERR, "Error on stop: [%s]: %d" , cmd_stop, ec1);
        }
        if (ec2 != 0) {
            syslog(LOG_ERR, "Error on start: [%s]: %d", cmd_start, ec2);
        }
        if (ec1 == 0 && ec2 == 0) {
            syslog(LOG_INFO, "Network Restarted");
            return 1;
        }
    }
    return 0;
}

int main() /* int argc, char *argv[]) */
{
    int last_n;
    long last_mt;

    sigusr1 = 0;
    last_n = sigusr1;
    daemonization();
    signal(SIGUSR1, &sigusr_handler);
    signal(SIGUSR2, &sigusr2_handler);
    start_time = time(NULL);

    /* the systemd already appends the pid to the /var/log/syslog */
    syslog(LOG_INFO, "Start logging [pid=%d]", getpid());
    update_pid_file();
    if (!mk_backup()) {
        exit(1); /* cannot create backup file */
    }
    last_mt = get_last_modifedtime(nw_config);
    #ifdef DEBUG
    syslog(LOG_INFO, "[DEBUG] Loop >");
    #endif
    /* work loop */
    while(1) {
        long mt;
        sleep(IDLE_INTERVAL);
        #ifdef DEBUG
        syslog(LOG_INFO, "[DEBUG] Wakeup for check config lm");
        #endif
        if (sigusr1 > last_n) {
            #ifdef DEBUG
            syslog(LOG_INFO, "[DEBUG] Recived SIGUSR1");
            #endif
            /* last_n = sigusr1; */
        }

        mt = get_last_modifedtime(nw_config);
        if (mt == last_mt) {
            #ifdef DEBUG
            syslog(LOG_INFO, "[DEBUG] No Changes");
            #endif
            last_n = sigusr1;
        } else {
            syslog(LOG_INFO, "Detected changes in %s", nw_config);
            if (sigusr1 == last_n) { /* case: wakeups by sigusr1 from sleep */
                syslog(LOG_INFO, "%s will be restored in %d sec",
                                  nw_config, WAIT_BEFORE_BACKUP);
                sleep(WAIT_BEFORE_BACKUP);
            }
            if (check_is_new_config_ok(&last_n)) {
                syslog(LOG_INFO, "New Config Accepted");
                mk_backup(); /* backup of new config */
            } else {
                restore_backup_and_restart_network();
            }
            last_mt = get_last_modifedtime(nw_config);
        }
    }

    #ifdef DEBUG
    syslog(LOG_INFO, "[DEBUG] Loop leave and exit");
    #endif
    return EXIT_SUCCESS;
}

