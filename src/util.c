#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <syslog.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "util.h"


int file_exists(const char *filename)
{
    struct stat buf;
    return (filename && stat(filename, &buf) == 0);
}

/**
 * src - from filename
 * dst - to filename
 * return 1 - on success
 */
int copy(const char *src, const char *dst)
{
    unsigned char buffer[BUFFER_SIZE] = {0};
    int rfd, wfd;

    if ((rfd = open(src, O_RDONLY)) == -1) {
        syslog(LOG_ERR, "Failed to open input file %s\n", src);
        return 0; /* false */
    }

    /* open file in write mode and already exists to overwrite */
    if ((wfd = open(dst, O_WRONLY | O_CREAT | O_TRUNC, 644)) == -1) {
        syslog(LOG_ERR, "Failed to create output file %s\n", dst);
        close(rfd);
        return 0;
    }

    while (1) {
        ssize_t byte = read(rfd, buffer, sizeof(buffer));
        /* error with reading */
        if (byte == -1) {
            syslog(LOG_ERR, "Encountered an error\n");
            break;
        } else if (byte == 0) {
        #ifdef DEBUG
            syslog(LOG_INFO, "[DEBUG] File copying successful.\n");
        #endif
            break;
        }

        if (write(wfd, buffer, byte) == -1) {
            syslog(LOG_ERR, "Failed to copying file\n");
            break;
        }
    }

    close(rfd);
    close(wfd);
    return 1;
}

long get_last_modifedtime(const char *path)
{
    struct stat attr;
    stat(path, &attr);
    return attr.st_mtime;
    /* printf("Last modified time: %s", ctime(&attr.st_mtime)); */
}
