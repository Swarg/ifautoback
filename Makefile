BINARY=ifautoback
SRC_DIR=./src
BUILD_DIR=./build
CC=gcc
INCDIRS=-I.
OPT=-O0
CFLAGS=-g -Wall -ansi -pedantic -Wextra $(INCDIRS) $(OPT)
SRCS=$(shell find $(SRC_DIRS) -name '*.c' -or -name '*.s')
OBJS=$(SRCS:%=$(BUILD_DIR)/%.o)

all: $(BUILD_DIR)/$(BINARY)

$(BUILD_DIR)/$(BINARY): $(OBJS)
	$(CC) $(OBJS) -o $@ $(LDFLAGS)

$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@


# with sudo: `sudo make install`
install: $(BUILD_DIR)/$(BINARY)
	install $(BUILD_DIR)/$(BINARY) /usr/bin/$(BINARY)
	install ./ifautobackctl /usr/bin/
	install ./ifautoback.service /etc/systemd/system/
	chmod 0644 "/etc/systemd/system/ifautoback.service"
	@echo "Installed. You can use:"
	@echo "sudo systemctl daemon-reload"
	@echo "sudo systemctl enable --now ifautoback.service"
	@echo "sudo systemctl status ifautoback"
	@echo	"sudo tail /var/log/syslog"
	@echo	"sudo ifautobackctl [start|status|stop|config_ok]"

# with sudo
uninstall:
	systemctl stop ifautoback.service
	systemctl disable ifautoback.service
	rm /usr/bin/$(BINARY)
	rm /usr/bin/ifautobackctl
	rm /etc/systemd/system/ifautoback.service
	@echo "Uninstalled"

# with sudo. Use it after updating network config
config_ok:
	kill -SIGUSR1 $(shell cat /var/run/ifautoback.pid | tr -d '\n')

# Check is really installed
check:
	md5sum /usr/bin/$(BINARY)
	md5sum $(BUILD_DIR)/$(BINARY)
	ls -lh /etc/systemd/system/ifautoback.service

clean:
	rm -rf ./build/
