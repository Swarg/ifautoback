#!/bin/bash
# Goal: break specific interface
# =============================================================================
# !! WARNING !! WARNING !! WARNING !! WARNING !! WARNING !! WARNING !! WARNING
#
#       THIS SCRIPT FOR TEST ONLY! IT BREAKS /etc/network/interfaces
#
# !! WARNING !! WARNING !! WARNING !! WARNING !! WARNING !! WARNING !! WARNING
# =============================================================================
#
# run with sudo

if [ "$1" == "" -o "$1" == "--help" ]; then
  echo "Usage: <interface-name>"
  exit 0
fi

# Check is given exists interface name
HAS=$(ip addr | grep "$1")
if [ "$HAS" == "" ]; then
  echo "Not Found [$1]  with  'ip addr | grep $1'"
  exit 1
fi

# make backups
LINES=$(cat /etc/network/interfaces | wc -l)
if [ "$LINES" != "1" ]; then
  if [ -f /tmp/interfaces.back ]; then
    cp /tmp/interfaces.back /tmp/interfaces.back1
  fi
  cp /etc/network/interfaces /tmp/interfaces.back
fi

ipdown "$1"
/etc/init.d/networking stop
echo "#Empty" > /etc/network/interfaces
/etc/init.d/networking start

