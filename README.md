# ifautoback

A simple host-side service that restore network settings then after the changes
there is no confirmation of its acceptance from the user.

## Goal
Provide a way to automatically restore the network setup to a previous
working configuration. When you need to change the network configuration on
a remote host and this changes could potentially break the network
and block access to the remote host.

## Behavior briefly
- tracks the update of the network config file.
- if service detects changes, wait for confirmation to accept the new configuration.
- if service does not receive confirmation, it restore the network configuration
  to the previous working state.
- continue


## Behavior detailed
- tracked configuration file: `/etc/network/interfaces`
- on start: create backup of tracked file and remember the time of its modification
- go to the infinity loop
- wake up every minute and check if there have been config changes by checking
  last_modified (time) of the tracked file
- if the config-file has been changed - wait for confirmation (SIGUSR1).
- if confirmation from user has not been received:
   - save the new not-confirmed-config-file to the cache-dir with *.new postfix.
   - replace the current config-file by the previous working configuration (from backup).
   - restart Network
- if confirmation received - accept the new config-file and make the new backup
- continue tracking for new changes


The frequency of checking the config file for changes - `one minute`.

The time given to the user to confirm the acceptance of the new configuration
is `two minutes`.

If after the service detects changes of the config and two minutes have elapsed
and no confirmation is received from the user, service consider that the new
configuration has broken the network, so the network settings will be restored
to the previous working state.

 Thus, automatic restoration of access to the network of a remote host will
occur in 2-3 minutes.


## Installation

```bash
git clone https://gitlab.com/Swarg/ifautoback
cd ifautoback
sudo make install
```

Make only install the application as systemd service,  but does not starting
and not enable this service.

To enable and start the service after installation use:
```bash
sudo systemctl daemon-reload
sudo systemctl enable --now ifautoback.service
sudo systemctl status ifautoback.service
```

`daemon-reload` - after update any file in /etc/systemd/system/
the files have to be reloaded

`systemclt enable` - to enable your service on every reboot.

`enable --now` - starting the service, with --now you start and enable service immediately.


## Installation: Manually
```bash
cd  ifautoback
make build
sudo cp ./build/ifautoback /usr/bin/ifautoback
sudo cp ./ifautobackctl /usr/bin/ifautobackctl
sudo cp ./ifautoback.service /etc/systemd/system/
sudo chmod 644 /etc/systemd/system/fautoback.service
```

## Uninstall

```bash
cd ifautoback
sudo make uninstall
```

# Usage

```bash
sudo systemctl status ifautoback.service
sudo systemctl stop ifautoback.service
sudo systemctl restart ifautoback.service

# Get pid of running daemon-process
sudo cat /var/run/ifautoback.pid
```

## ifautobackctl
ifautobackctl - the cli for interact with the ifautoback.service

```bash
sudo ifautobackctl --help
```
`enable|disable|start|stop|status|reload|pid|config-ok`


## Confirm acceptance of the new configuration

To confirm acceptance of the updated network configuration, use
```bash
  sudo ifautobackctl config-ok
```
OR
```bash
  kill -SIGUSR1 $(cat /var/run/ifautoback.pid | tr -d '\n')
```

`TODO`: Find a way to automatically checks if the new configuration breaks
the network setup.


#### The Backup of latest stable network config
`/var/cache/ifautoback/interfaces.back`

#### The Not accepted new network config
`/var/cache/ifautoback/interfaces.new`

(Which was automatically replaced by the previous working)</br>
This config, the acceptance of which was not confirmed by the user.
Since there was no confirmation from user, it is considered that it broke
the network setting. And so it was replaced by the last working config from backup.
This can be useful when you made changes, but did not have time to confirm their
acceptance, and the service automatically restore the new one by previous
worked from backup.


## How to see the logs:
```bash
sudo cat /var/log/syslog
```
OR
```bash
sudo systemctl status ifautoback.service
```


## Autorestart ifautoback.service

`WARN` By Default service configured as `Restart=always`.
This means that you cannot stop the service through `sudo kill -9 <PID>`
Use `sudo systemclt stop ifautoback` or `sudo ifautobackctl stop` instead.

Note that when the service starts it consider the current network config file -
as working network configuration and creates a backup based on it for subsequent
recovery. At the same time overwriting the old backup.


## Commands used by the service to restart a network setup

This commands tested and correctly works in Debian-based system.
It can not work with another network-services.
you can manually edit this commands in source file [src/main.c](./src/main.c)

```c
/* Commands to restrat Network: /etc/network/interfaces */
const char *nw_stop = "/etc/init.d/networking stop";
const char *nw_start = "/etc/init.d/networking start";
```



# Additional. Network Commands

## How Manually reload interface
Then, the procedure to to turn off eth0 interface is as follows
(replace the eth0 with your actual name). Run:
```bash
ifdown eth0
```

To turn on eth0 interface run:
```bash
ifup eth0
```

See ip address info using the ip command:
`ip a show eth0`

If you get error such as `ifup or ifdown command not found`,
then use the `ip` command to turn off and on interface named eth0:

```bash
sudo ip link set enp0 down
sudo ip link set enp0 upoal
```

### Restating networking service on Alpine Linux

Use the service command:
```
service networking restart
```
OR #
```
/etc/init.d/networking restart
```

Refs:
[how-to-restart-network-interface-in-debian-based-linux-without-being-restarting](https://stackoverflow.com/questions/52171796/how-to-restart-network-interface-in-debian-based-linux-without-being-restarting)
