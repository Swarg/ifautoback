#!/bin/bash

REMOTE="zv:~/dev/docker/ifautoback/"

if [ "$1" != "" ]; then
  REMOTE="$1"
fi

scp {Makefile,README.md,ifautobackctl,ifautoback.service} "$REMOTE/"
scp ./src/* "$REMOTE/src/"
